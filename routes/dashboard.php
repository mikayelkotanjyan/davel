<?php

use Illuminate\Support\Facades\Route;


    Route::get('/', 'DashboardController@index')->name('dashboard');

    Route::resource('stores', 'StoreController');
    Route::get('/get-stores/data', 'StoreController@dataTable')->name('stores.dataTable');
    Route::get('/get-nearest-stores', 'StoreController@getNearestStores')->name('stores.getNearestStores');
