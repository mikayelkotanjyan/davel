{!! Form::label($name, $display_name ?? __(ucfirst(str_replace('_', ' ', str_replace('_id', '', $name))))) !!}
{!! Form::select($name . ((isset($multiple) && $multiple) ? '[]' : ''), $data, $selected ?? null, ['id' => $id??'', 'class' => 'form-control', 'multiple' => $multiple ?? false]) !!}
{!! $errors->first($name, '<span class="form-text text-danger">:message</span>') !!}

@if(isset($select_2) && $select_2)
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>

    <script>

    var select_2_tags = false;
    @if(isset($tags) && $tags)
        select_2_tags = true;
    @endif
    $('#{{$id??''}}').select2({
        tags: select_2_tags
    });
</script>
@endif

