{!! Form::label($label ?? $name, __(ucfirst(str_replace('_', ' ', $label ?? $name)))) !!}
{!! Form::password($name, null, ['id' => $name, 'class' => 'form-control', 'placeholder' => __(ucfirst(str_replace('_', ' ', $label ?? $name)))]) !!}
{!! isset($errors) ? $errors->first($name, '<span class="form-text text-danger">:message</span>') : '' !!}

<script>
    $('#password').addClass('form-control');
    $('#password').attr('placeholder', '{{ ucfirst(str_replace('_', ' ', $label ?? $name)) }}');
</script>