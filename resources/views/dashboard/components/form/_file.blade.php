<div class="input-group" id="fileDiv">
    <span class="input-group-btn">
    <a id="{{$name}}_lfm" data-input="{{$name}}_thumbnail" data-preview="{{$name}}_holder" class="btn btn-primary text-white fileLink">
        <i class="flaticon2-photo-camera" style="color: #fff"></i> Выберите файл</a>
    </span>
    <input id="{{$name}}_thumbnail" class="form-control fileText" type="text" readonly value="{{ ($value && is_array($value) || is_object($value)) ? collect($value)->first() : $value }}">
    <input type="file" name="{{$name}}@isset($array)[]@endisset" id="{{$name}}_lfm_hid" class="form-control fileHidden"
           accept="" style="display: none" @isset($multiple) {{'multiple'}} @endisset>
</div>

{!! $errors->first($name, '<span class="form-text text-danger">:message</span>') !!}
{!! $errors->first($name . '.*', '<span class="form-text text-danger">:message</span>') !!}


{{--@isset($value)--}}
{{--    @if(is_array($value) || is_object($value))--}}
{{--        <br/>--}}
{{--        @foreach ($value as $val)--}}
{{--            <img src="{{$val}}" width="100" height="100" alt="file">--}}
{{--        @endforeach--}}
{{--    @else--}}
{{--        <img src="{{$value}}" width="200" alt="file">--}}
{{--    @endif--}}
{{--@endisset--}}

@section('scripts')
    <script>
        $('#fileDiv a').each(function(elem, obj) {
            $('#' + obj.id).on('click', function() {
            $('#' + obj.id + '_hid').click();
          });
        })


        $('input[type="file"]').each(function(elem, obj) {
          $('#' + obj.id).on('change', function() {
            var value =$('#' + obj.id).val();
            var name = obj.name;
            var name = ( name.includes('[') && name.includes(']') ) ? obj.name.replace('[]','') : obj.name;
            $('#' + name + '_thumbnail').val(value);
          });
        })

    </script>
@endsection


