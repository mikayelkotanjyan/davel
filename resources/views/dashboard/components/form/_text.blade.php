{!! Form::label($label ?? $name, __(ucfirst(str_replace('_', ' ', $label ?? $name)))) !!}
{!! Form::text($name, $value ?? null, ['id' => $name, 'class' => 'form-control', 'placeholder' => __(ucfirst(str_replace('_', ' ', $placeholder ?? '')))]) !!}
{!! isset($errors) ? $errors->first($name, '<span class="form-text text-danger">:message</span>') : '' !!}
