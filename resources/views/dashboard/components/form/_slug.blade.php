{!! Form::label($name, __('dashboard.Friendly URL')) !!}
<div class="input-group">
    <span class="slugSpan">{{$text}}</span>
    <input class="form-control" type="text" name="{{$name}}" value="{{$value}}">
    <span class="slugSpan">/</span>
</div>
@section('css')
    <style>
        .slugSpan {
            background: lightgray;
            color: white;
            padding: 10px;
            border-radius: 3px;
        }
    </style>
@endsection
