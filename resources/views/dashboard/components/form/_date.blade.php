{!! Form::label($name, __(ucfirst(str_replace('_', ' ', $name)))) !!}
<input type="date" name="{{$name}}" id="{{$name}}" required class="form-control"  placeholder="{{__(ucfirst(str_replace('_', ' ', $name)))}}" value="{{(old($name)) ? old($name) : (isset($object) && $object ? \Carbon\Carbon::parse($object)->format('Y-m-d') : null)}}">
{!! $errors->first($name, '<span class="form-text text-danger">:message</span>') !!}
