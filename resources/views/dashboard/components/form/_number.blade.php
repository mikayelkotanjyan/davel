
    {!! Form::label($label ?? $name, __(ucfirst(str_replace('_', ' ', $label ?? $name)))) !!}
    {!! Form::number($name, null, ['id' => $id ?? '', 'class' => 'form-control', 'placeholder' => __(ucfirst(str_replace('_', ' ', $placeholder ?? '')))]) !!}
    {!! $errors->first($name, '<span class="form-text text-danger">:message</span>') !!}
