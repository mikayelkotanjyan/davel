<label class="switch mr-3">
    <input type="checkbox" name="{{$name}}" class="{{$class ?? ''}}">
    <span class="slider"></span>
</label>
{!! Form::label($name, __(ucfirst(str_replace('_', ' ', $label ?? $name)))) !!}

<style>
    .switch {
        position: relative;
        display: inline-block;
        width: 55px;
        height: 25px;
    }

    .switch input {
        display: none;
    }

    .slider {
        position: absolute;
        cursor: pointer;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        background-color: #e8ebf1;
        width: 57px;
        height: 30px;
        border-radius: 15px;
        transition: .4s;
    }

    .slider:before {
        position: absolute;
        content: '';
        background-color: white;
        color: #fff;
        height: 24px;
        width: 24px;
        left: 3px;
        bottom: 3px;
        border-radius: 50%;
        transition: .4s;

    }

    input:checked + .slider:before {
        transform: translateX(26px);
        font-family: LineAwesome;
        text-decoration: inherit;
        text-rendering: optimizeLegibility;
        text-transform: none;
        content: '\f17b';
        background-color: #5d78ff;
        color: #fff;
        padding: 4px;
    }

    .switch::before {
        content: "";
        display: block;
        margin-bottom: 5px;
    }

    .switch label {
        color: black;
        font-size: 14px;
        font-weight: bold;
    }

</style>

{{--@section('scripts')--}}
    <script>
        $(document).on('change', '.delay', function() {
            if ($(this).is(':checked')) {
                $('.delayInput').show();
            } else {
                $('.delayInput').hide();
            }
        });

        $(document).on('change', '.testMode', function() {
            if ($(this).is(':checked')) {
                $('.testInput').show();
            } else {
                $('.testInput').hide();
            }
        });
    </script>
{{--@endsection--}}