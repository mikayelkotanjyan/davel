{!! Form::label($name, __(ucfirst(str_replace('_', ' ', $label ?? $name)))) !!}
{!! Form::textarea($name, $value ?? null, ['id' => $name, 'class' => 'form-control', 'rows' => $rows ?? 5, 'placeholder' => __(ucfirst(str_replace('_', ' ', $label ?? $name)))]) !!}
{!! isset($errors) ? $errors->first($name, '<span class="form-text text-danger">:message</span>') : '' !!}

@if(isset($editor) && $editor == true)
    <script>

        CKEDITOR.replace('{{$name}}',
            {

            }
        );

    </script>
@endif
