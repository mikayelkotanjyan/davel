@extends('dashboard.layouts.app')

@section('content')
    <div class="kt-subheader  kt-grid__item" id="kt_subheader">
        <div class="kt-container  kt-container--fluid ">
            <div class="kt-subheader__main">
                <h3 class="kt-subheader__title">Dashboard</h3>
                <span class="kt-subheader__separator kt-subheader__separator--v"></span>
                <div class="kt-subheader__breadcrumbs">
                    <a href="{{route('dashboard')}}" class="kt-subheader__breadcrumbs-home"><i
                            class="flaticon2-shelter"></i></a>
                    <span class="kt-subheader__breadcrumbs-separator"></span>
                    <a href="{{route('dashboard')}}" class="kt-subheader__breadcrumbs-link">Dashboard</a>
                </div>
            </div>
        </div>
    </div>


    <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">

        <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
            <div class="row">

                <div class="col-xl-12 col-lg-12">

                    <div class="kt-portlet kt-portlet--height-fluid">
                        <div class="kt-portlet__body">
                            <h1>Welcome</h1>
                            <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid mt-3">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="kt-portlet">
                                            <div class="kt-portlet__head">
                                                <div class="kt-portlet__head-label">
                                                    <h3 class="kt-portlet__head-title">
                                                        Get Nearest Stores
                                                    </h3>
                                                </div>
                                            </div>
                                            <div class="kt-portlet__body border-12">
                                                <div class="row">
                                                    <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                                        <div class="form-group">
                                                            @include('dashboard.components.form._text', (['name' => 'latitude', 'value' => null, 'label' => 'Latitude']))
                                                        </div>
                                                    </div>

                                                    <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                                        <div class="form-group">
                                                            @include('dashboard.components.form._text', (['name' => 'longitude', 'value' => null, 'label' => 'Longitude']))
                                                        </div>
                                                    </div>

                                                    <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 d-flex align-items-center">
                                                        <div class="form-group mb-0">
                                                           <button type="button" class="btn btn-primary search">Search</button>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="kt-datatable kt-datatable--default kt-datatable--brand kt-datatable--loaded w-100">
                                                        <table class="table-responsive-lg kt-datatable__table dataTable" id="table">
                                                            <thead>
                                                                <tr>
                                                                    <th></th>
                                                                    <th>Name</th>
                                                                    <th>Description</th>
                                                                    <th>Address</th>
                                                                </tr>
                                                            </thead>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

            </div>
        </div>

    </div>

@endsection

@section('scripts')
    <script>
        $('.dataTable').DataTable({
            responsive: true,
            columns: [
                {data: 'distance', visible: false},
                {data: 'name'},
                {data: 'description'},
                {data: 'address'},
            ],
        });

        $('.search').click(function () {
            let latitude = $(this).closest('.row').find('[name="latitude"]').val();
            let longitude = $(this).closest('.row').find('[name="longitude"]').val();
            $.get( "/dashboard/get-nearest-stores", { latitude: latitude, longitude: longitude} )
                .done(function(response) {
                    var table = $('#table').DataTable();
                    table.clear();
                    table.rows.add(JSON.parse(response));
                    table.draw();
                });
        })
    </script>
@endsection
