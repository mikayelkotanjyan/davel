<span style="overflow: visible; position: relative; width: 110px;white-space: nowrap">
     <a href="{{route('stores.show', $store->id)}}" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="Show Details">
            <i class="flaticon-eye"></i>
     </a>
    <a href="{{route('stores.edit', $store->id)}}" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="Edit details">
          <i class="flaticon-edit"></i>
    </a>
    <form action="{{route('stores.destroy', $store->id)}}" method="POST" style="display: none" onsubmit="return confirm('Are you sure?')">
        @csrf
        @method('DELETE')
    </form>
    <a href="#" onclick="$(this).prev().submit()" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="Delete">
         <i class="flaticon2-trash"></i>
    </a>
</span>
