@extends('dashboard.layouts.app')
@section('content')
    <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">

        <div class="kt-subheader  kt-grid__item" id="kt_subheader">
            <div class="kt-container  kt-container--fluid align-items-center">
                <div class="kt-subheader__main">
                    <h3 class="kt-subheader__title">Stores</h3>
                    <span class="kt-subheader__separator kt-subheader__separator--v"></span>
                    <div class="kt-subheader__breadcrumbs">
                        <a href="{{route('dashboard')}}" class="kt-subheader__breadcrumbs-home"><i
                                    class="flaticon2-shelter"></i></a>
                        <span class="kt-subheader__breadcrumbs-separator"></span>
                        <a href="{{route('stores.index')}}" class="kt-subheader__breadcrumbs-link">
                            Stores</a>
                        <span class="kt-subheader__breadcrumbs-separator"></span>
                        <a href="{{route('stores.edit', $store->id)}}"
                           class="kt-subheader__breadcrumbs-link">
                            Edit Store - {{$store->id}} </a>
                    </div>
                </div>
                <div class="">
                    <a href="" class="btn btn-secondary j_return_back">Cancel</a>
                    <button type="submit" class="btn btn-primary" id="object-form-confirm">Update</button>
                </div>
            </div>
        </div>

        <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="kt-portlet">
                        <div class="kt-portlet__head">
                            <div class="kt-portlet__head-label">
                                <h3 class="kt-portlet__head-title">
                                    Edit Store - {{$store->id}}
                                </h3>
                            </div>
                        </div>
                        {!! Form::model($store, [
                         'method' => 'PATCH',
                         'files' => true,
                         'id' => 'object-form',
                         'class' => 'kt-form kt-form--label-right',
                         'url' => route('stores.update',$store->id),
                       ]) !!}

                        @csrf
                        @include('dashboard.store.form')
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

