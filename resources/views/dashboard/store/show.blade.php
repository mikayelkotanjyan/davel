@extends('dashboard.layouts.app')
@section('content')
    <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">

        <div class="kt-subheader  kt-grid__item" id="kt_subheader">
            <div class="kt-container  kt-container--fluid align-items-center">
                <div class="kt-subheader__main">
                    <h3 class="kt-subheader__title">Stores</h3>
                    <span class="kt-subheader__separator kt-subheader__separator--v"></span>
                    <div class="kt-subheader__breadcrumbs">
                        <a href="{{route('dashboard')}}" class="kt-subheader__breadcrumbs-home"><i
                                    class="flaticon2-shelter"></i></a>
                        <span class="kt-subheader__breadcrumbs-separator"></span>
                        <a href="{{route('stores.index')}}" class="kt-subheader__breadcrumbs-link">
                            Stores  </a>
                        <span class="kt-subheader__breadcrumbs-separator"></span>
                        <a href="{{route('stores.show', $store->id)}}" class="kt-subheader__breadcrumbs-link">
                            Store - {{$store->id}} </a>
                    </div>
                </div>

                <form action="{{route('stores.destroy', $store->id)}}" method="post"
                      onsubmit="return confirm('Are you sure?')" class="mr-1">
                    {{method_field('delete')}}
                    {{csrf_field()}}
                    <button type="submit" class="btn btn-danger btn-icon" title="Delete"><i class="flaticon2-trash"></i>
                    </button>
                </form>
            </div>
        </div>

        <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="kt-portlet kt-portlet--mobile">
                        <div class="kt-portlet__head kt-portlet__head--lg">
                            <div class="kt-portlet__head-label">
                                <h3 class="kt-portlet__head-title">
                                    Store - {{$store->id}}
                                </h3>
                            </div>
                        </div>

                        <div class="kt-portlet__body">
                            <table class="table card-table table-responsive-lg">
                                <tbody>
                                    <tr>
                                        <th>Name</th>
                                        <td><span class="text-muted">{{$store->name}}</span></td>
                                    </tr>

                                    <tr>
                                        <th>Address</th>
                                        <td><span class="text-muted">{{$store->address}}</span></td>
                                    </tr>

                                    <tr>
                                        <th>Description</th>
                                        <td><span class="text-muted">{{$store->description}}</span></td>
                                    </tr>
                                </tbody>
                            </table>

                        </div>
                        <div class="card-footer text-right">
                            <div class="d-flex">
                                <a href="#" class="btn btn-link j_return_back">Back</a>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection

