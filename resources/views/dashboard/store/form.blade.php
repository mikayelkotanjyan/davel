<div class="kt-portlet__body border-12">
    <div class="row">
        <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
            <div class="form-group">
                @include('dashboard.components.form._text', (['name' => 'name', 'value' => $store->name ?? null, 'label' => 'Name']))
            </div>
        </div>

        <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
            <div class="form-group">
                @include('dashboard.components.form._text', (['name' => 'address', 'value' => $store->address ?? null, 'label' => 'Address']))
            </div>
        </div>

        <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
            <div class="form-group">
                @include('dashboard.components.form._textarea', (['name' => 'description', 'value' => $store->description ?? null, 'label' => 'Description']))
            </div>
        </div>

    </div>
</div>
