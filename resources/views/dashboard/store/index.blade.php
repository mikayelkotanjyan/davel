@extends('dashboard.layouts.app')

@section('content')
    <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">

        <div class="kt-subheader  kt-grid__item" id="kt_subheader">
            <div class="kt-container  kt-container--fluid align-items-center">
                <div class="kt-subheader__main">
                    <h3 class="kt-subheader__title">Stores</h3>
                    <span class="kt-subheader__separator kt-subheader__separator--v"></span>
                    <div class="kt-subheader__breadcrumbs">
                        <a href="{{route('dashboard')}}" class="kt-subheader__breadcrumbs-home"><i
                                class="flaticon2-shelter"></i></a>
                        <span class="kt-subheader__breadcrumbs-separator"></span>
                        <a href="{{route('stores.index')}}" class="kt-subheader__breadcrumbs-link">
                            Stores </a>
                    </div>
                </div>
            </div>
        </div>

        <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="kt-portlet kt-portlet--mobile">
                        <div class="kt-portlet__head kt-portlet__head--lg">
                            <div class="kt-portlet__head-label">
                                <h3 class="kt-portlet__head-title">
                                    Stores
                                </h3>
                            </div>
                            <div class="kt-portlet__head-toolbar">
                                <div class="kt-portlet__head-wrapper">
                                    <div class="kt-portlet__head-actions">
                                        <a href="{{route('stores.create')}}" class="btn btn-brand btn-elevate btn-icon-sm">
                                            <i class="flaticon2-plus"></i>
                                            Create Store
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @include('flash::message')
                        <div class="kt-portlet__body">
                            <div class="kt-datatable kt-datatable--default kt-datatable--brand kt-datatable--loaded">
                                <table class="table-responsive-lg kt-datatable__table dataTable">
                                    <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Name</th>
                                            <th>Description</th>
                                            <th>Address</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script>
        $('.dataTable').DataTable({
            serverSide: true,
            processing: true,
            responsive: true,
            ajax: "{{route('stores.dataTable')}}",
            columns: [
                {data: 'id'},
                {data: 'name'},
                {data: 'description'},
                {data: 'address'},
                {
                    data: null,
                    render: function(data, type, row) {
                        return '<span style="overflow: visible; position: relative; width: 110px;white-space: nowrap">' +
                            '<a href="/dashboard/stores/' + row.id + '" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="Show Details"><i class="flaticon-eye"></i></a>' +
                            '<a href="/dashboard/stores/' + row.id + '/edit" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="Edit details"><i class="flaticon-edit"></i></a>' +
                            '<form action="/dashboard/stores/' + row.id + '" method="POST" style="display: none" onsubmit="return confirm(`Are you sure?`)">' +
                            '@csrf'+
                            '@method("DELETE")'+
                            '</form>' +
                            '<a href="#" onclick="$(this).prev().submit()" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="Delete"> <i class="flaticon2-trash"></i> </a> </span>';
                    },
                    orderable: false,
                    searchable: false
                }
            ],
        });
    </script>
@endsection


