<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Store extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'description',
        'address',
        'latitude',
        'longitude',
    ];

    public static function laratablesCustomActions($store)
    {
        return view('dashboard.store.custom._actions', compact('store'))->render();
    }
}
