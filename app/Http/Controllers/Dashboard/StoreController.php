<?php
namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\Store;
use Freshbitsweb\Laratables\Laratables;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Builder;
class StoreController extends Controller
{
    public function index()
    {
        return view('dashboard.store.index');
    }

    public function dataTable()
    {
        $stores = Store::all()->toArray();
        $data = [
            "draw" => 1,
            "recordsTotal" => count($stores),
            "recordsFiltered" => count($stores),
            "data" => $stores
        ];
        return json_encode($data);
    }

    public function create()
    {
        return view('dashboard.store.create');
    }

    public function store(Request $request)
    {
        $data = $request->all();
        $address = str_replace(' ', '+', $data['address']);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://maps.googleapis.com/maps/api/geocode/json?address='. $address .'&key=AIzaSyC1dNlmvqZYGI7cWZangz-nNm1kFApDrgQ');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 0);

        $headers = array();
        $headers[] = 'Accept: application/json';
        $headers[] = 'Content-Type: text/plain';
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $result = json_decode(curl_exec($ch));
        if (curl_errno($ch)) {
            echo 'Error:' . curl_error($ch);
        }
        curl_close($ch);

        if ($result) {
            $latitude = $result->results[0]->geometry->location->lat;
            $longitude = $result->results[0]->geometry->location->lng;
            $data['latitude'] = $latitude;
            $data['longitude'] = $longitude;
        }

        $store = Store::create($data);


        flash()->success('Store Created Successfully!');
        return redirect()->route('stores.index');
    }

    public function show($id)
    {
        $store = Store::findOrFail($id);
        return view('dashboard.store.show', compact('store'));
    }


    public function edit($id)
    {
        $store = Store::findOrFail($id);
        return view('dashboard.store.edit', compact('store'));
    }

    public function update(Request $request, $id)
    {
        $data = $request->all();
        $store = Store::find($id);
        if ($store->address != $data['address']) {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, 'https://maps.googleapis.com/maps/api/geocode/json?address=18+Mazmanyan+street,+Yerevan,+Armenia&key=AIzaSyC1dNlmvqZYGI7cWZangz-nNm1kFApDrgQ');
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_POST, 0);

            $headers = array();
            $headers[] = 'Accept: application/json';
            $headers[] = 'Content-Type: text/plain';
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

            $result = json_decode(curl_exec($ch));
            if (curl_errno($ch)) {
                echo 'Error:' . curl_error($ch);
            }
            curl_close($ch);

            if ($result) {
                $latitude = $result[0]['geometry']['location']['lat'];
                $longitude = $result[0]['geometry']['location']['lng'];
                $data['latitude'] = $latitude;
                $data['longitude'] = $longitude;
            }
        }
        $store->update($data);

        flash()->success('Updated Successfully!');
        return redirect()->route('stores.index');

    }

    public function destroy($id)
    {
        $store = Store::whereId($id)->first();
        if (!$store) {
            abort(404);
        }

        $store->delete();
        flash()->success('Store deleted!');
        return redirect()->route('stores.index');
    }

    public function getNearestStores(Request $request)
    {
        $latitude = $request->get('latitude');
        $longitude = $request->get('longitude');

        $stores =  Store::select(DB::raw("name,description,address,latitude,longitude, ( 6371 * acos( cos( radians('$latitude') ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians('$longitude') ) + sin( radians('$latitude') ) * sin( radians( latitude ) ) ) ) AS distance"))->havingRaw('distance < 50')->orderBy('distance')
            ->get()->toArray();

        return json_encode($stores);
    }
}
